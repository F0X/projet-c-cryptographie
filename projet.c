#include <stdlib.h>
#include <regex.h>
#include <string.h>
#include <stdio.h>

char* verif_alpha(char *texte);
//Pb : Chaine vide, accents ou caractères spéciaux.

char* convertir_accents(wchar_t *texte);
//Identifier tous les accents et trouver la  lettre associée

char chiffrer_Cesar(long cle,char *text_chiffrer);

char dechiffrer_Cesar(long cle,char *texte_clair);

char chiffrer_Vigenere(char *cle, char* text_chiffre);

char dechiffrer_Vigenere(char *cle, char* text_clair);
//Petit problème sous vigenere, le '\0' de fin de chaîne compte pour le cryptage..
//Nous avons donc choisi de ne pas crypter la lettre qui correspond à ce '\0'

void main() {
    printf("Veuillez entrer votre phrase : ");
    char *buffer = NULL;
    size_t taille_texte=0;
    int nb_char = 0;
    char *text_verif;
    nb_char = getline(&buffer,&taille_texte,stdin);//Avoir le texte et sa taille
    printf("%s\n",buffer);//Afficher le texte.
    text_verif = verif_alpha(buffer);//Vérifie la justesse du texte

    printf("Chiffrage de César \n");
    printf("Tapez 1 pour chiffrer un message ou tapez 2 pour déchiffrer \n");
    printf("Chiffrage de Vigenère \n");
    printf("Tapez 3 pour chiffrer un message ou tapez 4 pour déchiffrer \n");
    char *buffer3 = NULL;
    size_t taille_texte2=0;
    int nb_char_switch = 0;
    long choix;
    nb_char_switch = getline(&buffer3,&taille_texte2,stdin);
    choix=strtol(buffer3,NULL,10);
    switch (choix){
        case 1:
        printf("Veuillez entrer la clef (entier) : \n");
        char *buffer2 =NULL;
        size_t taille_cle=0;
        int nb_char_lu;
        nb_char_lu = getline(&buffer2,&taille_cle,stdin);//Avoir la clef
        long cle;
        cle=strtol(buffer2,NULL,10); //convertis la clef en long

        chiffrer_Cesar(cle, text_verif);

        printf("Voulez vous déchiffrer ce message? Si oui tapez 1 sinon tapez 2 :\n");
        char *buffer4 = NULL;
        size_t taille_texte3=0;
        int nb_char_switch2 = 0;
        long choix2;
        nb_char_switch2 = getline(&buffer4,&taille_texte3,stdin);
        choix2=strtol(buffer4,NULL,10);
        if (choix2 != 1){
        	break;
        }
        case 2:
        printf("Veuillez entrer la clef (entier) : \n");
        char *buffer5 =NULL;
        size_t taille_cle2=0;
        int nb_char_lu2;
        nb_char_lu2 = getline(&buffer5,&taille_cle2,stdin);//Avoir la clef
        long cle2;
        cle2 = strtol(buffer5,NULL,10); //convertis la clef en long
        dechiffrer_Cesar(cle2,text_verif);
        break;
        case 3:
        printf("Veuillez entrer la clef en minuscule (chaine de caractères) : \n");
        char *buffer6=NULL;
        size_t taille_cleV=0;
        int nb_char_luV;
        char *cle_V;
        nb_char_luV = getline(&buffer6,&taille_cleV,stdin);
        cle_V=buffer6;
        chiffrer_Vigenere(cle_V,text_verif);

        printf("Voulez vous déchiffrer ce message? Si oui tapez 1 sinon tapez 2 : \n");
        char *buffer7 = NULL;
        size_t taille_texte4=0;
        int nb_char_switch3 = 0;
        long choix3;
        nb_char_switch3 = getline(&buffer7,&taille_texte4,stdin);
        choix3 = strtol(buffer7,NULL,10);
        if (choix3 != 1){
        	break;
        }
        case 4:
        printf("Veuillez entrer la clef en minuscule (chaine de caractères) : \n");
        char *buffer8=NULL;
        size_t taille_cleV2=0;
        int nb_char_luV2;
        char *cle_V2;
        nb_char_luV = getline(&buffer8,&taille_cleV2,stdin);
        cle_V2 = buffer8;
        dechiffrer_Vigenere(cle_V2,text_verif);
        break;
    }

    
}

char* verif_alpha(char *texte){
    regex_t preg;
    int err;
    const char *str_request = texte;
    const char *str_regex = "[:upper:][[:alpha:]]+\\.";
    err = regcomp (&preg, str_regex, REG_NOSUB | REG_EXTENDED);
    if (err==0){
        int match;
        match = regexec(&preg, str_request, 0,NULL,0);
        regfree(&preg);
        if (match==0){
            printf("Le texte est valide\n");
            return texte;
        }
        else {
            printf("Le texte n'est pas valide\n");
            convertir_accents(texte);
            return texte;
        }
    }
    else {
        printf("Erreur execution\n");
        return texte;
    }
    //On test charactère par charactère si c'est une lettre
    //OU que c'est un apostrophe ou une virgule
    //si oui, on passe au charactère suivant, sinon on renvoie une erreur.
}



char *convertir_accents(wchar_t *texte){ //Nous n'avons pas pu finir cette fonction
	int iterateur=0;
	while (texte[iterateur] != '\0'){
		wprintf(L"Lettre : %lc\n", texte[iterateur]);
		texte[iterateur]='a';
		wprintf(L"Lettre après modif : %lc\n", texte[iterateur]);
		iterateur++;
	}
	//Convertir le wchar_t en char
	return texte;
}



char chiffrer_Cesar(long cle, char *text_chiffre){
    int iterateur=0;
    while(text_chiffre[iterateur] != '\0'){
        printf("Lettre avant modif : %c\n", text_chiffre[iterateur]);
        if (text_chiffre[iterateur]>='A' && text_chiffre[iterateur]<='Z'){
            text_chiffre[iterateur]=text_chiffre[iterateur]-'A'+cle;
            if (text_chiffre[iterateur]<0){
                text_chiffre[iterateur]+=26;
            }
            else if (text_chiffre[iterateur]>25){
                text_chiffre[iterateur]-=26;
            }
            text_chiffre[iterateur]+='A';
        }
        if (text_chiffre[iterateur]>='a' && text_chiffre[iterateur]<='z'){
            text_chiffre[iterateur]=text_chiffre[iterateur]-'a'+cle;
            if (text_chiffre[iterateur]<0){
                text_chiffre[iterateur]+=26;
            }
            else if (text_chiffre[iterateur]>25){
                text_chiffre[iterateur]-=26;
            }
            text_chiffre[iterateur]+='a';
        }
        printf("Lettre : %c\n", text_chiffre[iterateur]);
        iterateur++;
    }
    printf("%s\n",text_chiffre);
    return (*text_chiffre);
}


char dechiffrer_Cesar(long cle, char *text_clair){
    int iterateur=0;
    while(text_clair[iterateur] != '\0'){
        printf("Lettre avant modif : %c\n", text_clair[iterateur]);
        if (text_clair[iterateur]>='A' && text_clair[iterateur]<='Z'){
            text_clair[iterateur]=text_clair[iterateur]-'A'-cle;
            if (text_clair[iterateur]<0){
                text_clair[iterateur]+=26;
            }
            else if (text_clair[iterateur]>25){
                text_clair[iterateur]-=26;
            }
            text_clair[iterateur]+='A';
        }
        if (text_clair[iterateur]>='a' && text_clair[iterateur]<='z'){
            text_clair[iterateur]=text_clair[iterateur]-'a'-cle;
            if (text_clair[iterateur]<0){
                text_clair[iterateur]+=26;
            }
            else if (text_clair[iterateur]>25){
                text_clair[iterateur]-=26;
            }
            text_clair[iterateur]+='a';
        }
        printf("Lettre : %c\n", text_clair[iterateur]);
        iterateur++;
    }
    printf("%s\n",text_clair);
    return (*text_clair);
}


char chiffrer_Vigenere(char *cle, char *text_chiffre){
    int longueur_cle=strlen(cle);
    int decalage;
    int iterateur=0;
    while(text_chiffre[iterateur] != '\0'){
        printf("Lettre avant modif : %c\n",text_chiffre[iterateur]);
        decalage=(cle[iterateur%longueur_cle])-'a';
        printf("Décalage = %d\n",decalage);
        if (decalage>0){

	        if (text_chiffre[iterateur]>='A' && text_chiffre[iterateur]<='Z'){
	            if (decalage>0){
		            text_chiffre[iterateur]=(text_chiffre[iterateur]-'A'+decalage);
		            if (text_chiffre[iterateur]<0){
		                text_chiffre[iterateur]+=26;
		            }
		            else if (text_chiffre[iterateur]>25){
		                text_chiffre[iterateur]-=26;
		            }
		            text_chiffre[iterateur]+='A';
		        }
	        }
	        if (text_chiffre[iterateur]>='a' && text_chiffre[iterateur]<='z'){
	            text_chiffre[iterateur]=(text_chiffre[iterateur]-'a'+decalage);
	            if (text_chiffre[iterateur]<0){
	                text_chiffre[iterateur]+=26;
	            }
	            else if (text_chiffre[iterateur]>25){
	                text_chiffre[iterateur]-=26;
	            }
	            text_chiffre[iterateur]+='a';
	        }
	    }
        printf("Lettre : %c\n", text_chiffre[iterateur]);
        iterateur++;
    }
    printf("%s\n",text_chiffre);
    return (*text_chiffre);
}


char dechiffrer_Vigenere(char *cle, char* text_clair){
    int longueur_cle=strlen(cle);
    int decalage;
    int iterateur=0;
    while(text_clair[iterateur] != '\0'){
        printf("Lettre avant modif : %c\n",text_clair[iterateur]);
        decalage=(cle[iterateur%longueur_cle])-'a';
        printf("Décalage = %d\n",decalage);
        if (decalage>0){
	        if (text_clair[iterateur]>='A' && text_clair[iterateur]<='Z'){
	            text_clair[iterateur]=(text_clair[iterateur]-'A'-decalage);
	            if (text_clair[iterateur]<0){
	                text_clair[iterateur]+=26;
	            }
	            else if (text_clair[iterateur]>25){
	                text_clair[iterateur]-=26;
	            }
	            text_clair[iterateur]+='A';
	        }
	        if (text_clair[iterateur]>='a' && text_clair[iterateur]<='z'){
	            text_clair[iterateur]=(text_clair[iterateur]-'a'-decalage);
	            if (text_clair[iterateur]<0){
	                text_clair[iterateur]+=26;
	            }
	            else if (text_clair[iterateur]>25){
	                text_clair[iterateur]-=26;
	            }
	            text_clair[iterateur]+='a';
	        }
	    }
        printf("Lettre : %c\n", text_clair[iterateur]);
        iterateur++;
    }
    printf("%s\n",text_clair);
    return (*text_clair);
}

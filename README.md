# Projet C cryptographie

réalisé par GEY Nathanaël et LOMBARD Florian

L'objectif de cette application est de chiffrer un message commençant
par une majuscule et finissant par un point.
L'utilisateur entre sa phrase puis choisit le mode de cryptage qu'il souhaite
utiliser (César ou Vigenère).
Ensuite il doit rentrer la clef de chiffrage, un nombre pour César et un mot
en minuscule pour Vigenère.
Enfin le code déchiffre le message codé avec la même clef de chiffrement.

Nous n'avons pas eu le temps de finaliser l'application avec la gestion des accents
comme le code ne compilait pas nous avons décidé de vous fournir un code complet 
qui compile mais sans la gestion d'accents.

Nos fonctions :
char* verif_alpha(char *texte);
#Pour vérifier qu'il n'y ai que des caractères alpha-numérique dans la phrase et
que celle ci corresponde à nos critères. 
En entrée nous avons le texte initial qui est modifié directement si besoin 
dans la fonction grâce à l'utilisation du pointeur.
Si nous avions réussis à faire la gestion d'accent cette fonction aurait un wchar_t en entrée et sortie qui redirigerait vers "convertir_accents" en cas de lecture d'accent

char* convertir_accents(wchar_t *texte);
#Convertis les accents en lettre alphanumérique (par exemple le "é" en "e")

char chiffrer_Cesar(long cle,char *text_chiffrer);
#Pour chiffrer un message avec le cryptage de César.
En entrée nous avons le texte à crypter et la clef de chiffrement.

char dechiffrer_Cesar(long cle,char *texte_clair);
#Pour déchiffrer un message sous le cryptage de César
En entrée il y a le message à déchiffrer et la clef de chiffrement.

char chiffrer_Vigenere(char *cle, char* text_chiffre);
#Pour chiffrer un message avec le cryptage de Vigenère.
En entrée nous avons le texte à crypter et la clef de chiffrement.

char dechiffrer_Vigenere(char *cle, char* text_clair);
#Pour déchiffrer un message sous le cryptage de Vigenère.
En entrée il y a le message à déchiffrer et la clef de chiffrement.


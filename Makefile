SRC = projet.c

OBJ = $(SRC:.c = .o)

NAME = projet

$(NAME): $(OBJ)
	gcc -o $(NAME) $(OBJ)
all: $(NAME)

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)
re: fclean all

